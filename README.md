# Projet_3-Groupe_1

Anthony, Arnaud, Gabriel, Kamel
Lien du jupiter notebook : https://simplon-dev-data.gitlab.io/grenoble-2020-2021/projets/projet_3/projet_3-groupe_1/#/
# Description du dépôt GitLab

- Ce fichier README.md, contenant la description du dépôt GitLab
- Un dossier files_txt/ contenant plusieurs fichiers de notes.
    - Un fichier idees.txt, un vrac à idées.
    - Un fichier liste_sites_emploi.txt contenant des infos sur les sites principaux considérés.
    - Un fichier notes.txt. (nom explicite)
    - Un fichier references.txt. (nom explicite)
- Un dossier gestion_de_projet/ contenant les fichiers à propos de la gestion du projet.
    - Une image sprint_1.png du planning du sprint 1.
    - Une image sprint_2.png du planning du sprint 2.
- Deux fichiers Pipfile & Pipfile.lock pour la configuration de l'environnement virtuel utilisé pour le projet.
- Un fichier docker-compose.yml avec la configuration du container docker postgreSQL utilisé pourn le projet.
- Un fichier .gitignore contenant les fichiers locaux à ne pas partager.
- Un fichier arborescence_py.md contenant un graphique présentant la hiérarchie des scripts Python du projet.
- Un fichier config_api_google_search contenant les informations nécessaires pour la configuration de l'API Google (nécessaire pour éxecuter le script).
- Un fichier Jupyter Notebook main.ipynb permettant d'éxecuter tous les scripts.
- Un dossier modules_python/ contenant tous les scripts Python, en formats `.py` et `.ipynb`.
- Un dossier old/ contenant la production désuette.

# Description du projet

Une fois le fichier de configuration `config_api_google_search.py` complété avec les deux variables permettant l'utilisation de l'API google search, le script `main.py` fait appel aux fonctions des modules pour :
1. scrapper l'ensemble du dataset choisi pour le projet et l'injecter dans une table postgreSQL.
2. exporter le dataset en format csv (`offres.csv`)
3. visualiser chaque offre datant de moins de 30 jours via une cartographie.
4. visualiser la répartition des offres selon le poste via un 'radar'.
5. visualiser la répartition des pré-requis d'expérience selon le pooste via un diagramme à barres.  

Ce script `main.py` est intégré en continu à l'aide de GitLab Pages.

### Pipeline

![Image pipeline.](./reflexion_et_conception_projet/pipeline_donnees.png "Image du pipeline.")

Pour pouvoir utiliser les scripts, une clé d'API google ainsi d'un ID custom search engine sont nécessaires.  
Pour renseigner ces valeurs, les ajouter dans le cadre ci-dessous _(entre les `""`)_ &  
copier le contenu de ce fichier.md dans le dossier principal du projet en local sous  
&nbsp;&nbsp;&nbsp;&nbsp;le nom : config_api_google_search.py  
----------------------------------------------------------------------------------------------------------

```python
#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Renseigner entre les guillemets les valeurs respectives
# de la clef d'API google et l'identifiant du custom search engine
# Pour obtenir une clé API google : https://console.cloud.google.com/home/ > "Accéder à l'aperçu des API" > "Identifiants"
# Pour obtenir un identifiant de custom search engine : https://programmablesearchengine.google.com/cse
my_api_key = ""
my_cse_id = ""
```
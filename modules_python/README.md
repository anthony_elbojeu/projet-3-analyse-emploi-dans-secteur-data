# relations "parent-enfant" des scripts python

> le script main est situé dans la racine du repo
> les autres scripts sont rangés dans le dossier `modules_python`

```mermaid
classDiagram
    main <|-- pipeline_pole_emploi : modules_python/
    main <|-- visualisations : modules_python/
    pipeline_pole_emploi <|-- scripts : pole_emploi/
    main : pipeline_pole_emploi(liste_mots_clefs)
    pipeline_pole_emploi : creation_dict_fonction(liste_mots_clefs)
    pipeline_pole_emploi : dict_to_dataframe
    pipeline_pole_emploi : dataframe_to_dbms
    scripts : creation_dict_fonction
    scripts : creation_liste_lien
    scripts : creation_liste_page
    scripts : collecte_coordonnees_entreprise
    scripts : collecte_date_offre
    scripts : collecte_description
    scripts : collecte_experience_offre
    scripts : collecte_intitule
    scripts : collecte_nom_entreprise
    scripts : collecte_type_contrat
    scripts : collecte_ville_offre
    visualisations : visualisation_cartographie
    visualisations : visualisation_pie_experience
    visualisations : visualisation_radar_mots_clefs
```

#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import psycopg2
from psycopg2.extensions import parse_dsn
from sqlalchemy import create_engine

from modules_python import pole_emploi


# In[ ]:


def pipeline_pole_emploi(liste_mots_clefs):
    # Création du dictionnaire
    import time
    start = time.time()
    
    pour_df_fonction=pole_emploi.creation_dict_fonction(liste_mots_clefs)
    
    end = time.time()
    time_taken = round(float(end-start), 2)
    min_taken = str(int(time_taken//60))
    sec_taken = str(int(time_taken%60))
    print(min_taken+"min"+sec_taken+"s taken to complete job.")
    
    
    # Création du dataframe
    df = pd.DataFrame(pour_df_fonction, dtype="string")
#     df = df.drop_duplicates()
    
    
    # parse_dsn
    db_dsn = "postgres://postgres:test@localhost:5432/Offre_Emploi"
    db_args = parse_dsn(db_dsn)
    
    
    # Connexion et drop table si nécessaire, puis close connexion
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()
    cur.execute("""DROP TABLE IF EXISTS raw""")
    conn.commit()

    
    # Exportation du dataframe dans la base de données
    engine = create_engine(db_dsn)
    df.to_sql('raw', engine, if_exists='replace', index = False)
    
    
    # Attribution de la contrainte Unique à la colonne "lien_URL"
    cur.execute("""CREATE TABLE IF NOT EXISTS raw_history AS SELECT * FROM raw""")
    cur.execute("""TRUNCATE raw_history""")
    cur.execute("""ALTER TABLE raw_history DROP CONSTRAINT IF EXISTS unique_all""")
    cur.execute("""ALTER TABLE raw_history ADD CONSTRAINT unique_all UNIQUE ("motCle",
                                                                            "intitulé",
                                                                            "ville",
                                                                            "numero_offre",
                                                                            "duree_contrat",
                                                                            "type_contrat")""")
    conn.commit()
    
    cur.execute("""INSERT INTO raw_history SELECT * FROM raw ON CONFLICT DO NOTHING""")
    conn.commit()
    
    
    cur.close()
    conn.close()


# In[ ]:





#!/usr/bin/env python
# coding: utf-8

# In[2]:


import re

from bs4 import BeautifulSoup


# In[3]:


def date_offre(soup):
    date_offre=soup.find('p',class_="t5 title-complementary")
    if date_offre is None:
        publie = None
        date = None
        numero_offre = None
    else:
        matching = re.search(r'(Publié|Actualisé) le (.*)\n\n- offre n°\n([0-9]+)',date_offre.text)
        publie, date, numero_offre = matching.groups()
    return publie, date, numero_offre


# In[ ]:





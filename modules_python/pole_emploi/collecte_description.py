#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from bs4 import BeautifulSoup


# In[1]:


def description(soup):
    description_offre = soup.find('div',class_="description col-sm-8 col-md-7")
    if description_offre is None:
        descriptiondeloffre= None
    else:
        descriptiondeloffre=description_offre.text
    return(descriptiondeloffre)


# In[ ]:





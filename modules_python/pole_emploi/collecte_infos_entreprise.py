#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from bs4 import BeautifulSoup
from googleapiclient.discovery import build
import config_api_google_search


# In[13]:


def infos_entreprise(soup,limit):
    my_api_key = config_api_google_search.my_api_key
    my_cse_id = config_api_google_search.my_cse_id
    limit_local = limit
    entreprise_name = soup.select_one('#contents > div.container-fluid.gabarit-full-page.with-large-right-column > div > div.panel-center.col-md-8 > div > div.modal-details.modal-details-offre > div > div > div > div > div.media > div > h4')
    entreprise_lien = soup.select_one('#contents > div.container-fluid.gabarit-full-page.with-large-right-column > div > div.panel-center.col-md-8 > div > div.modal-details.modal-details-offre > div > div > div > div > div.media > div.media-body > dl > dd > a')
    if entreprise_name is not None:
        nom_entreprise = entreprise_name.text
    else:
        nom_entreprise = None
    if entreprise_lien is not None:
        lien_entreprise = entreprise_lien.text
    elif entreprise_name is not None:
        if limit_local < 100:
            print('-=-=-   try number', str(limit_local), '@ getting society\'s link from google API   -=-=-')
            print('nom de l\'entreprise', nom_entreprise)
            limit_local += 1
            try:
                service = build("customsearch", "v1", developerKey=my_api_key)
                lien_entreprise = service.cse().list(q=nom_entreprise, cx=my_cse_id).execute()['items'][0]['link'].strip()
            except:
                print('-o-o- Google API limit reached - appending None -o-o-')
                lien_entreprise = None
        else:
                lien_entreprise = None
    else:
        lien_entreprise = None
    return(nom_entreprise, lien_entreprise, limit_local)


# In[ ]:





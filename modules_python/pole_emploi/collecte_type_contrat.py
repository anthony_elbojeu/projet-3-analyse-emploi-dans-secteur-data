#!/usr/bin/env python
# coding: utf-8

# In[2]:

import re

from bs4 import BeautifulSoup


# In[3]:


def type_contrat(soup):
    type_contrat = soup.find('dl',class_="icon-group").contents[1]
    if type_contrat is None:
        duree_contrat = None
        type_contrat = None
    else:
        matching = re.match(r'\n(.*)\n\n(.*)\n', type_contrat.text)
        duree_contrat, type_contrat = matching.groups()
    return duree_contrat, type_contrat


# In[ ]:





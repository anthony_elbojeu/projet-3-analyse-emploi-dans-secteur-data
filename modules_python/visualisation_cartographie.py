#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import locale
from datetime import datetime, timedelta

import folium
from folium import plugins
import pandas as pd
import psycopg2
from psycopg2.extensions import parse_dsn
from sqlalchemy import create_engine


# In[ ]:


#####################################################################################################
####               VERSION SQL
######################################################################

def carto_SQL(str_colonne_de_regroupement):
    locale.setlocale(locale.LC_ALL, '')
    
    db_dsn = "postgres://postgres:test@localhost:5432/Offre_Emploi"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()  
    
    data_frame_visu=pd.read_sql("""SELECT "lien_URL", "url_entreprise", "intitulé",
                                    "entreprise", "ville", "longitude",
                                    "latitude", "date_publication" from raw""",conn)
    conn.commit()

    centre_coordonnees = (46.227638, 2.213749)

    carte = folium.Map(location=centre_coordonnees, zoom_start=5)

    liste_entreprises = []
    
    ####################
    ## Transformation des dates
    ####################
    
    dt=[]
    for elt in data_frame_visu["date_publication"]:
        
        if elt is not None:
            date_pr_traitement=datetime.strptime(elt, "%d %B %Y").date()
            dt.append(date_pr_traitement)
        else:
            dt.append(elt)
    
    data_frame_visu["date_publication"] = dt
    
    ######################
    ## On ne garde que les dates dans les 30 derniers jours
    ###################
    
    end_date = datetime.today().date()
    
    start_date = end_date - timedelta(days=30)

    after_start_date = data_frame_visu["date_publication"] >= start_date

    before_end_date = data_frame_visu["date_publication"] <= end_date

    between_two_dates = after_start_date & before_end_date

    filtered_dates = data_frame_visu.loc[between_two_dates]
    
    donnees_groupees = filtered_dates.groupby(str_colonne_de_regroupement)
    
    for str_col in donnees_groupees.groups:
        groupe = donnees_groupees.get_group(str_col)
        emplacements = []
        descriptions = []
        icones = []
        
        i=0
        for _,elt in groupe.iterrows():
            lien_url = elt['lien_URL']
            entreprise = elt['entreprise']
            ville=elt['ville']
            longitude = elt['longitude']
            latitude = elt['latitude']
            intit=elt['intitulé']
            en_url = elt['url_entreprise']
            if i == 0:
                mc = folium.plugins.MarkerCluster(
                    name=str_col,
                    location = ([np.float64(latitude),np.float64(longitude)])
                ).add_to(carte)
                i+=1
                
            if pd.notna(latitude) and pd.notna(longitude):
                acces = "Accéder au site de l\'entreprise"
                if not en_url:
                    en_url = ""
                    acces = "Pas de lien vers l\'entreprise."
                azeoioiu = '<a href="'+en_url+f'"target=_blank>{acces}</a>'
                folium.Marker(
                    location = ([np.float64(latitude),np.float64(longitude)]),
                    tooltip = intit,
                    popup = (
                        '<a href="'+lien_url+f'"target=_blank>Accéder à l\'offre</a><br>'\
                        +'<a href="'+en_url+f'"target=_blank>{acces}</a>'
                                 if en_url
                                 else '<a href="'+lien_url+f'"target=_blank>Accéder à l\'offre</a><br>'\
                        +acces
                    )
                ).add_to(mc)
            
    folium.LayerControl().add_to(carte)
    
    return(carte)


# In[ ]:





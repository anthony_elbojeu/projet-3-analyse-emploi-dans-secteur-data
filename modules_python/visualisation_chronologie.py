#!/usr/bin/env python
# coding: utf-8

# In[1]:


import locale
from datetime import datetime,timedelta

import psycopg2
from psycopg2.extensions import parse_dsn
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# In[24]:


###########################################
############### VERSION BARPLOT
############################################


def chronologie_barplot(col_regroupement):
    
    locale.setlocale(locale.LC_ALL,'fr_FR.UTF-8')
    
    db_dsn = "postgres://postgres:test@localhost:5432/Offre_Emploi"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    
    data_frame_chronologie=pd.read_sql("""SELECT "date_publication", "motCle" from raw ORDER BY "date_publication" asc""",conn)
    
    colonnes_a_garder=["date_publication", "motCle"]
    
    
    
    donnees_propres = data_frame_chronologie[colonnes_a_garder]

    # Transformation des dates du format numérodujour Mois ANNEE au format de date anglaises
    
    dt=[]
    for elt in donnees_propres["date_publication"]:
        
        if elt is not None:
            date_pr_traitement=datetime.strptime(elt, "%d %B %Y").date()
            dt.append(date_pr_traitement)
        else:
            dt.append(elt)
    donnees_propres["date_publication"] = dt
    
    ######################
    #### ne garder que les dates depuis 30
    ###############
    
    end_date = datetime.today().date()
    
    start_date = end_date - timedelta(days=30)

    after_start_date = donnees_propres["date_publication"] >= start_date

    before_end_date = donnees_propres["date_publication"] <= end_date

    between_two_dates = after_start_date & before_end_date

    filtered_dates = donnees_propres.loc[between_two_dates]
    
    ################
    #####
    ###############
    
    donnees_groupees = filtered_dates.groupby(col_regroupement,sort=False)
    
    
    
    # dico_mot_cle est un dictionnaire contenant comme clé les mots clés, chaque clé (mots clés)
    # doit être remplie avec la série indicée par les dates et qui compte le nombre d'offres 
    # publiées pour chaque date 
    
    dico_mot_cle=dict()
    
    # Initialisation de la liste des labels
    
    line_labels=[]
    
    for don_group_par_mots_cle in donnees_groupees.groups:
    # Récupérer le groupe de la date en cours en cours (ou d'une autre colonne de regroupement)
        
        # Ajout des titres de légendes qui correspondent aux mots clé
        line_labels.append(don_group_par_mots_cle)
        
        groupe_par_mot_cle = donnees_groupees.get_group(don_group_par_mots_cle)
        
        # chaque groupe contient la date de publication pour chaque motCle, 
       
        
        compte=groupe_par_mot_cle["date_publication"].value_counts(sort=False)
        
        # on obtient une serie indexée par la date et qui comptabilise les offres pour chaque date 
        # qui représente l'index
        
        # il fautà trier la liste par date croissante
        compte_trie=compte.sort_index()
        
        # ajout de la série dans le dictionnaire dans la clé correspondante don_group_par_mots_cle
        
        #dico_mot_cle[don_group_par_mots_cle]=serie_formattee
        dico_mot_cle[don_group_par_mots_cle]=compte_trie
    
    dico_df=pd.DataFrame(dico_mot_cle)
    
    
    
    
    ##################
    # Normalisation du dataframe
    ##################
    
    #df_normal=dico_df.div(dico_df.sum(axis=1), axis=0)
    #print('dico_df')
    #print(df_normal)
    
    ############################
    # Création du graphe avec les pourcentages
    #############################
    #https://stackoverflow.com/questions/51495982/display-totals-and-percentage-in-stacked-bar-chart-using-dataframe-plot
    
    # insertion de la date comme colonne au début de la dataframe
    
    dico_df.insert(0, 'date_publication', dico_df.index)
    
    ####### 
    df_total = dico_df.sum(axis=1)
    
    #############
    ### Remplacement de Nan par 0
    #############
    
    dico_df=dico_df.fillna(0)
    
    ##############
    #### Calcul de la moyenne et du nombre d'offres par mois
    ##############
    
    somme_total=df_total.sum()
    print('Nombre d\'offres ce mois-ci: '+ str(int(somme_total)))
    moyenne_jour=somme_total/30
    print('Nombre moyen d\'offres publiées en une journée: '
          +str(round(moyenne_jour))+ ' ('+str(round(moyenne_jour,1))+' arrondie)')
    
    
    ############
    # Première version qui marche quand les barres sont horizontales
    ############
  
    
    fig=dico_df.plot(x = 'date_publication', kind='barh',stacked = True,
                     mark_right = True, figsize=(20, 20) )
    

    df_rel = dico_df[dico_df.columns[1:]].div(df_total, 0)*100
    
    
    for n in df_rel:
        for i, (cs, ab, pc, tot) in enumerate(zip(dico_df.iloc[:, 1:].cumsum(1)[n], dico_df[n], df_rel[n], df_total)):
            plt.text(tot, i, "    "+str(int(tot))+ " offre(s) au total", va='center', rotation=0, fontsize=11.5)
            if pc > 0:
                plt.text(np.round(cs - ab/2,2), i, str(int(np.round(pc,0))) + '%', va='center', ha='center', rotation=0)
    
    fig.legend([x[5:].capitalize() for x in dico_df.columns[1:]], bbox_to_anchor=(-0.22, 1), loc='upper left')
    
     #plt.xaxis.set_major_formatter(date_form)
    #date_form = DateFormatter("%d %B %Y")
    labels=[]
    for x in dico_df['date_publication']:
        labels.append(datetime.strptime(str(x), '%Y-%m-%d').strftime('%d %B %Y'))
    
    fig.set_title('Calendrier de publication des offres (30 derniers jours)', fontsize=40)
    fig.set_yticks(np.arange(len(dico_df['date_publication'])))
    fig.set_yticklabels(labels, fontsize=11.5)
    #fig.yaxis.set_major_formatter(date_form)
    ###############################
    ################## VERSION BARPLOT
    ################################


# In[ ]:





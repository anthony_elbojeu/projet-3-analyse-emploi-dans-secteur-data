#!/usr/bin/env python
# coding: utf-8

# In[58]:


import numpy as np

import matplotlib as m
import matplotlib.pyplot as plt
import plotly.express as px
import pandas as pd
import psycopg2
from psycopg2.extensions import parse_dsn

import warnings
warnings.filterwarnings('ignore')

from mpldatacursor import datacursor
import mpld3
from mpld3 import plugins


# In[65]:


def pie_chart():
    
    db_dsn = "postgres://postgres:test@localhost:5432/Offre_Emploi"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()
    
    df=pd.read_sql("""SELECT CASE
                                            WHEN "experience" = '0'
                                                THEN 'a_Débutant accepté'
                                            WHEN "experience" > '0'
                                            AND "experience" <= '1.0'
                                                THEN 'c_1 an ou moins'
                                            WHEN "experience" > '1.0'
                                            AND "experience" <= '2.0'
                                                THEN 'd_2 ans ou moins'
                                            WHEN "experience" > '2.0'
                                            AND "experience" <= '3.0'
                                                THEN 'e_3 ans ou moins'
                                            WHEN "experience" > '3.0'
                                            AND "experience" <= '4.0'
                                                THEN 'f_4 ans ou moins'
                                            WHEN "experience" > '4.0'
                                            AND "experience" <= '5.0'
                                                THEN 'g_5 ans ou moins'
                                            WHEN "experience" > '5.0'
                                                THEN 'h_Plus de 5 ans'
                                            WHEN "experience" IS NULL
                                                THEN 'i_Non précisé'
                                            ELSE
                                                'Case non anticipated'
                                            END AS "Expérience demandée",
                                            COUNT(*) AS "val",
                                            CASE WHEN "motCle" = 'data+scientist' THEN 'Scientist'
                                            WHEN "motCle" = 'data+ingénieur' THEN 'Ingénieur'
                                            WHEN "motCle" = 'data+engineer' THEN 'Engineer'
                                            WHEN "motCle" = 'data+développeur' THEN 'Développeur'
                                            WHEN "motCle" = 'data+consultant' THEN 'Consultant'
                                            WHEN "motCle" = 'data+analyst' THEN 'Analyst'
                                            ELSE 'Error manipulating keyword'
                                            END AS "mot_cle"
                                            FROM raw
                                            GROUP BY "Expérience demandée",
                                                        "mot_cle"
                                            ORDER BY "Expérience demandée" """,conn)
    conn.commit()

    # dict = e.g.:{mot_cle: int(total_d'occurences)}
    sommes = {}
    for mot_clef in pd.unique(df['mot_cle']):
        sommes[mot_clef] = (df.loc[df['mot_cle'] == mot_clef].get('val').sum())

    # make every val a percent
    for i in range(len(df['val'])):
        df['val'][i] = (df['val'][i]*100 / sommes[df['mot_cle'][i]])

    df = df.pivot(index="mot_cle", columns="Expérience demandée", values='val')

    
#     cm = m.colors.LinearSegmentedColormap('my_colormap', plt.colormaps['Blues'], 1024)
    
    df.plot(kind='barh',
            figsize = (20,10),
            stacked = True,
            fontsize=26,
            cmap='Blues')
    plt.title('Expérience demandée selon le poste proposé', fontsize=48)
    plt.ylabel('Mot clef de la recherche', fontsize=36)
    plt.xlabel('%', fontsize=36)
    plt.legend([x[2:] for x in df.columns],
               loc='best',
               bbox_to_anchor=(1.0, 1.025),
               fontsize=20)


# In[ ]:





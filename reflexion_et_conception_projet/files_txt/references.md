### Catalogue des API pôle-emploi -=- API offres -emploi main
https://www.emploi-store-dev.fr/portail-developpeur/detailapicatalogue/-offres-d-emploi-v2;jsessionid=61tyZa5ptxJL85sQW2uGbZ-4UVqDqslTvsfeuDMcZTQpbeHVvOKp!-303936922?id=5ba49d55243a5f9d2c5064a2
### Python doc API pôle-emploi
https://pypi.org/project/api-offres-emploi/
### Python scraping
https://stackoverflow.com/questions/39322134/n-in-beautiful-soup-text
https://www.crummy.com/software/BeautifulSoup/bs4/doc/
https://stackoverflow.com/questions/6150108/how-to-pretty-print-html-to-a-file-with-indentation
https://stackoverflow.com/questions/18725760/beautifulsoup-findall-given-multiple-classes
https://stackoverflow.com/questions/47100398/beautifulsoup-returning-none-when-element-definitely-exists
### Python API google
https://fr.python-requests.org/en/latest/
https://stackoverflow.com/questions/22623798/google-search-with-python-requests-library
https://www.hebergementwebs.com/nouvelles/comment-gratter-google-avec-python
https://www.programmez.com/magazine/article/interroger-google-avec-python
https://linuxhint.com/google_search_api_python/
https://pypi.org/project/gsearch/
### site web Mappy pour récupérer les coordonnées des annonces pôle-emploi
https://fr.mappy.com/
### doc matplotlib
https://matplotlib.org/
https://seaborn.pydata.org/generated/seaborn.histplot.html
https://www.geeksforgeeks.org/stacked-percentage-bar-plot-in-matplotlib/
### doc matplotlib Pie
https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.pie.html
### Stack Overflow pour avoir des réponses aux questions qu'on se pose
https://stackoverflow.com/
### Mermaid doc
https://mermaid-js.github.io/mermaid/#/
### Mermaid editor
https://mermaid-js.github.io/mermaid-live-editor/
### doc modules Python
https://docs.python.org/fr/3.5/tutorial/modules.html
https://docs.python.org/fr/3/reference/import.html
### doc gestion projet 
https://www.nutcache.com/fr/blog/creer-un-product-backlog/
### pour gérer les données manquantes
https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html
### Python doc API pôle-emploi
https://pypi.org/project/api-offres-emploi/
### très bon exemple de illustré de visualisation 
https://matplotlib.org/3.3.3/gallery/pie_and_polar_charts/pie_features.html#sphx-glr-gallery-pie-and-polar-charts-pie-features-py
### limitation de geopy
https://geopy.readthedocs.io/en/stable/
### lien vers les dictionnaires json des régions, départements et villes
https://www.data.gouv.fr/fr/datasets/regions-departements-villes-et-villages-de-france-et-doutre-mer/
### pour réaliser des représentations (séries choronologiques)
https://www.delftstack.com/fr/howto/matplotlib/matplotlib-plot-time-series/
### gérer les dates en français
https://www.geeksforgeeks.org/python-sort-list-of-dates-given-as-strings/
### manipuler/ordonner des series
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.sort_values.html
https://stackoverflow.com/questions/41433765/pandas-dataframe-sort-by-date
http://www.python-simple.com/python-pandas/creation-series.php
### ne pas ordonner lors des regroupements sur un dataframe
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.groupby.html
### ajuster la légende du graphe
https://riptutorial.com/matplotlib/example/9964/legend-placed-outside-of-plot
### définir le format des dates lors de la visualisation
https://www.earthdatascience.org/courses/use-data-open-source-python/use-time-series-data-in-python/date-time-types-in-pandas-python/customize-dates-matplotlib-plots-python/
### normaliser un dataframe
https://xspdf.com/resolution/53575822.html
### SQL
https://sql.sh/cours/case
https://tapoueh.org/blog/2013/07/simple-case-for-pivoting-in-sql/
### crontab helper
https://crontab.guru/
